/* tslint:disable */
/* eslint-disable */
/**
 * Admin.NET 通用权限开发平台
 * 让 .NET 开发更简单、更通用、更流行。整合最新技术，模块插件式开发，前后端分离，开箱即用。<br/><u><b><font color='FF0000'> 👮不得利用本项目从事危害国家安全、扰乱社会秩序、侵犯他人合法权益等法律法规禁止的活动！任何基于本项目二次开发而产生的一切法律纠纷和责任，我们不承担任何责任！</font></b></u>
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import globalAxios, { AxiosResponse, AxiosInstance, AxiosRequestConfig } from 'axios';
import { Configuration } from '../configuration';
// Some imports not used depending on template conditions
// @ts-ignore
import { BASE_PATH, COLLECTION_FORMATS, RequestArgs, BaseAPI, RequiredError } from '../base';
import { AdminResultString } from '../models';
import { AlipayPreCreateInput } from '../models';
import { AlipayTradePagePayInput } from '../models';
/**
 * AlipayApi - axios parameter creator
 * @export
 */
export const AlipayApiAxiosParamCreator = function (configuration?: Configuration) {
    return {
        /**
         * 
         * @summary 交易预创建
         * @param {AlipayPreCreateInput} [body] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        apiAlipayAlipayPreCreatePost: async (body?: AlipayPreCreateInput, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/api/alipay/alipayPreCreate`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions :AxiosRequestConfig = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Bearer required
            // http bearer authentication required
            if (configuration && configuration.accessToken) {
                const accessToken = typeof configuration.accessToken === 'function'
                    ? await configuration.accessToken()
                    : await configuration.accessToken;
                localVarHeaderParameter["Authorization"] = "Bearer " + accessToken;
            }

            localVarHeaderParameter['Content-Type'] = 'application/json-patch+json';

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.params) {
                query.set(key, options.params[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};
            const needsSerialization = (typeof body !== "string") || localVarRequestOptions.headers['Content-Type'] === 'application/json';
            localVarRequestOptions.data =  needsSerialization ? JSON.stringify(body !== undefined ? body : {}) : (body || "");

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary 统一收单下单并支付页面接口
         * @param {AlipayTradePagePayInput} [body] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        apiAlipayAlipayTradePagePayPost: async (body?: AlipayTradePagePayInput, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/api/alipay/alipayTradePagePay`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions :AxiosRequestConfig = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Bearer required
            // http bearer authentication required
            if (configuration && configuration.accessToken) {
                const accessToken = typeof configuration.accessToken === 'function'
                    ? await configuration.accessToken()
                    : await configuration.accessToken;
                localVarHeaderParameter["Authorization"] = "Bearer " + accessToken;
            }

            localVarHeaderParameter['Content-Type'] = 'application/json-patch+json';

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.params) {
                query.set(key, options.params[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};
            const needsSerialization = (typeof body !== "string") || localVarRequestOptions.headers['Content-Type'] === 'application/json';
            localVarRequestOptions.data =  needsSerialization ? JSON.stringify(body !== undefined ? body : {}) : (body || "");

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary 获取授权信息
         * @param {string} [userId] 用户Id
         * @param {string} [authCode] 授权码
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        apiAlipayGetAuthInfoGet: async (userId?: string, authCode?: string, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/api/alipay/getAuthInfo`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions :AxiosRequestConfig = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Bearer required
            // http bearer authentication required
            if (configuration && configuration.accessToken) {
                const accessToken = typeof configuration.accessToken === 'function'
                    ? await configuration.accessToken()
                    : await configuration.accessToken;
                localVarHeaderParameter["Authorization"] = "Bearer " + accessToken;
            }

            if (userId !== undefined) {
                localVarQueryParameter['UserId'] = userId;
            }

            if (authCode !== undefined) {
                localVarQueryParameter['AuthCode'] = authCode;
            }

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.params) {
                query.set(key, options.params[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary 支付回调
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        apiAlipayNotifyPost: async (options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/api/alipay/notify`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions :AxiosRequestConfig = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Bearer required
            // http bearer authentication required
            if (configuration && configuration.accessToken) {
                const accessToken = typeof configuration.accessToken === 'function'
                    ? await configuration.accessToken()
                    : await configuration.accessToken;
                localVarHeaderParameter["Authorization"] = "Bearer " + accessToken;
            }

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.params) {
                query.set(key, options.params[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
    }
};

/**
 * AlipayApi - functional programming interface
 * @export
 */
export const AlipayApiFp = function(configuration?: Configuration) {
    return {
        /**
         * 
         * @summary 交易预创建
         * @param {AlipayPreCreateInput} [body] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async apiAlipayAlipayPreCreatePost(body?: AlipayPreCreateInput, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => Promise<AxiosResponse<AdminResultString>>> {
            const localVarAxiosArgs = await AlipayApiAxiosParamCreator(configuration).apiAlipayAlipayPreCreatePost(body, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs :AxiosRequestConfig = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * 
         * @summary 统一收单下单并支付页面接口
         * @param {AlipayTradePagePayInput} [body] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async apiAlipayAlipayTradePagePayPost(body?: AlipayTradePagePayInput, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => Promise<AxiosResponse<AdminResultString>>> {
            const localVarAxiosArgs = await AlipayApiAxiosParamCreator(configuration).apiAlipayAlipayTradePagePayPost(body, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs :AxiosRequestConfig = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * 
         * @summary 获取授权信息
         * @param {string} [userId] 用户Id
         * @param {string} [authCode] 授权码
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async apiAlipayGetAuthInfoGet(userId?: string, authCode?: string, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => Promise<AxiosResponse<void>>> {
            const localVarAxiosArgs = await AlipayApiAxiosParamCreator(configuration).apiAlipayGetAuthInfoGet(userId, authCode, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs :AxiosRequestConfig = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * 
         * @summary 支付回调
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async apiAlipayNotifyPost(options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => Promise<AxiosResponse<AdminResultString>>> {
            const localVarAxiosArgs = await AlipayApiAxiosParamCreator(configuration).apiAlipayNotifyPost(options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs :AxiosRequestConfig = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
    }
};

/**
 * AlipayApi - factory interface
 * @export
 */
export const AlipayApiFactory = function (configuration?: Configuration, basePath?: string, axios?: AxiosInstance) {
    return {
        /**
         * 
         * @summary 交易预创建
         * @param {AlipayPreCreateInput} [body] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async apiAlipayAlipayPreCreatePost(body?: AlipayPreCreateInput, options?: AxiosRequestConfig): Promise<AxiosResponse<AdminResultString>> {
            return AlipayApiFp(configuration).apiAlipayAlipayPreCreatePost(body, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary 统一收单下单并支付页面接口
         * @param {AlipayTradePagePayInput} [body] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async apiAlipayAlipayTradePagePayPost(body?: AlipayTradePagePayInput, options?: AxiosRequestConfig): Promise<AxiosResponse<AdminResultString>> {
            return AlipayApiFp(configuration).apiAlipayAlipayTradePagePayPost(body, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary 获取授权信息
         * @param {string} [userId] 用户Id
         * @param {string} [authCode] 授权码
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async apiAlipayGetAuthInfoGet(userId?: string, authCode?: string, options?: AxiosRequestConfig): Promise<AxiosResponse<void>> {
            return AlipayApiFp(configuration).apiAlipayGetAuthInfoGet(userId, authCode, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary 支付回调
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async apiAlipayNotifyPost(options?: AxiosRequestConfig): Promise<AxiosResponse<AdminResultString>> {
            return AlipayApiFp(configuration).apiAlipayNotifyPost(options).then((request) => request(axios, basePath));
        },
    };
};

/**
 * AlipayApi - object-oriented interface
 * @export
 * @class AlipayApi
 * @extends {BaseAPI}
 */
export class AlipayApi extends BaseAPI {
    /**
     * 
     * @summary 交易预创建
     * @param {AlipayPreCreateInput} [body] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof AlipayApi
     */
    public async apiAlipayAlipayPreCreatePost(body?: AlipayPreCreateInput, options?: AxiosRequestConfig) : Promise<AxiosResponse<AdminResultString>> {
        return AlipayApiFp(this.configuration).apiAlipayAlipayPreCreatePost(body, options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * 
     * @summary 统一收单下单并支付页面接口
     * @param {AlipayTradePagePayInput} [body] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof AlipayApi
     */
    public async apiAlipayAlipayTradePagePayPost(body?: AlipayTradePagePayInput, options?: AxiosRequestConfig) : Promise<AxiosResponse<AdminResultString>> {
        return AlipayApiFp(this.configuration).apiAlipayAlipayTradePagePayPost(body, options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * 
     * @summary 获取授权信息
     * @param {string} [userId] 用户Id
     * @param {string} [authCode] 授权码
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof AlipayApi
     */
    public async apiAlipayGetAuthInfoGet(userId?: string, authCode?: string, options?: AxiosRequestConfig) : Promise<AxiosResponse<void>> {
        return AlipayApiFp(this.configuration).apiAlipayGetAuthInfoGet(userId, authCode, options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * 
     * @summary 支付回调
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof AlipayApi
     */
    public async apiAlipayNotifyPost(options?: AxiosRequestConfig) : Promise<AxiosResponse<AdminResultString>> {
        return AlipayApiFp(this.configuration).apiAlipayNotifyPost(options).then((request) => request(this.axios, this.basePath));
    }
}
